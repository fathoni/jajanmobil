<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>MYAUTO.ID</title>
    <link rel="icon" href="{{asset('logo/icon.png')}}">
    
    <!-- Stylesheets -->
    <!-- Bootstrap is included in its original form, unaltered -->
    <link rel="stylesheet" href="{{asset('proui/backend/css/bootstrap.min.css') }}">

    <!-- Related styles of various icon packs and plugins -->
    <link rel="stylesheet" href="{{asset('proui/backend/css/plugins.css') }}">

    <!-- The main stylesheet of this template. All Bootstrap overwrites are defined in here -->
    <link rel="stylesheet" href="{{asset('proui/backend/css/main.css') }}">

    <!-- Include a specific file here from css/themes/ folder to alter the default theme of the template -->
    <link id="theme-link" rel="stylesheet" href="{{asset('proui/backend/css/themes/night.css') }}">
    <!-- The themes stylesheet of this template (for using specific theme color in individual elements - must included last) -->
    <link rel="stylesheet" href="{{asset('proui/backend/css/themes.css') }}">
    <!-- END Stylesheets -->

    <!-- Modernizr (browser feature detection library) -->
    <script src="{{asset('proui/backend/js/vendor/modernizr.min.js') }}"></script>
    @yield('ex-js-up')
</head>
<body>
    <div id="page-container" class="sidebar-partial sidebar-visible-lg sidebar-no-animations">
        <div id="sidebar">
            <!-- Wrapper for scrolling functionality -->
            <div id="sidebar-scroll">
                <!-- Sidebar Content -->
                <div class="sidebar-content">
                    <!-- Brand -->
                    <a href="/" class="sidebar-brand" style="color: #48d5e9;">
                        <img src="{{asset('logo/new-logo.png')}}" width="100px" height="50px">
                    </a>
                    <!-- END Brand -->
                    <!-- User Info -->
                    <div class="sidebar-section sidebar-user clearfix sidebar-nav-mini-hide">
                        <div class="sidebar-user-avatar">
                            <a href="#">
                                <img src="{{asset('gambar/avatar.jpg')}}">
                            </a>
                        </div>
                        <div class="sidebar-user-name">{{ Auth::user()->name }}</div>
                    </div>
                    <!-- END User Info -->

                    <!-- Sidebar Navigation -->
                    <ul class="sidebar-nav">
                        <li>
                            <a href="{{url('admin/dashboard')}}"><i class="gi gi-stopwatch sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Dashboard</span></a>
                        </li>
                        <li>
                            <a href="{{url('admin/category')}}"><i class="gi gi-sort sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Category</span></a>
                        </li>
                        <li>
                            <a href="{{url('admin/brand')}}"><i class="gi gi-tag sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Brand</span></a>
                        </li>
                        <li>
                            <a href="{{url('admin/product')}}"><i class="gi gi-car sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Product</span></a>
                        </li>
                        <li>
                            <a href="{{url('admin/banner')}}"><i class="gi gi-pin_flag sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Banner</span></a>
                        </li>
                        <li>
                            <a href="{{url('admin/simulasi')}}"><i class="gi gi-hand_up sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Simulasi</span></a>
                        </li>
                        <li>
                            {{-- <a href="{{url('admin/kontak')}}"><i class="gi gi-envelope sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Inbox</span></a> --}}
                            <a href="{{url('admin/kontak')}}"><i class="gi gi-envelope sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Inbox</span><span class="sidebar-nav-indicator label label-indicator animation-floating" id="unread" style="background-color: #ff0000; opacity: 1;">0</span></a>
                        </li>
                        <li>
                            <a href="{{url('admin/info')}}"><i class="gi gi-tag sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Info</span></a>
                        </li>
                        <li>
                            <a href="{{url('admin/about')}}"><i class="gi gi-circle_info sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">About</span></a>
                        </li>
                        @if (Auth::user()->name == "admin")
                            <li>
                                <a href="{{url('admin/register')}}"><i class="fa fa-users sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Register</span></a>
                            </li>
                        @endif
                    </ul>
                    <!-- END Sidebar Navigation -->
                </div>
                <!-- END Sidebar Content -->
            </div>
            <!-- END Wrapper for scrolling functionality -->
        </div>
        <!-- END Main Sidebar -->
        <div id="main-container">
            <header class="navbar navbar-default">
                <!-- Left Header Navigation -->
                <ul class="nav navbar-nav-custom">
                    <!-- Main Sidebar Toggle Button -->
                    <li>
                        <a href="javascript:void(0)" onclick="App.sidebar('toggle-sidebar');this.blur();">
                            <i class="fa fa-bars fa-fw"></i>
                        </a>
                    </li>
                    <!-- END Main Sidebar Toggle Button -->
                </ul>
                <!-- END Left Header Navigation -->

                <!-- Right Header Navigation -->
                <ul class="nav navbar-nav-custom pull-right">
                    <!-- User Dropdown -->
                    <li class="nav-item dropdown">
                    <a class="dropdown-item" href="{{ route('logout') }}"
                        onclick="event.preventDefault();
                                    document.getElementById('logout-form').submit();">
                        {{ __('Logout') }}
                    </a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                </li>
                    <!-- END User Dropdown -->
                </ul>
                <!-- END Right Header Navigation -->
            </header>
            <!-- END Header -->
            <div id="page-content">
                @yield('content')
            </div>
        </div>
    </div>
    <script src="{{asset('proui/backend/js/vendor/jquery.min.js') }}"></script>
    <script src="{{asset('proui/backend/js/vendor/bootstrap.min.js') }}"></script>
    <script src="{{asset('proui/backend/js/plugins.js') }}"></script>
    <script src="{{asset('proui/backend/js/app.js') }}"></script>
    <script>
        $.ajax({
            url:'unread-message',
            method:'GET',
            success : function(response){
                $('#unread').text(response);
            }
        })
    </script>
</body>
</html>