@extends('template.index')   
@section('content')
<style>
.modal-open {
padding-right: 0 !important;
}
body {
    padding: 0 !important;
}
</style> 
        @if(Session::has('alert-success'))
            <div class="alert alert-success">
                <strong>{{ \Illuminate\Support\Facades\Session::get('alert-success') }}</strong>
            </div>
        @endif    
        <div class="content-header">
            <ul class="nav-horizontal text-center">
                <li>
                    <a href="/admin/product"><i class="fa fa-bar-chart"></i> Products</a>
                </li>
                <li>
                    <a href="/admin/category"><i class="gi gi-shop_window"></i> Category</a>
                </li>
                <li>
                    <a href="{{url('admin/brand')}}"><i class="gi gi-shopping_cart"></i> Brand</a>
                </li>
             
            </ul>
        </div>
        <!-- END Datatables Header -->
        <!-- Quick Stats -->
        {{-- <div class="row text-center">
            <!-- Regular Modal 2 -->
            <div id="modal-regular2" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <form action="{{ route('brand.store') }}" method="post" enctype="multipart/form-data">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <h3 class="modal-title">Brand Form</h3>
                            </div>
                            <div class="modal-body">                                
                                {{ csrf_field() }}     
                                <div class="form-group text-left">
                                    <label for="brand_name">Nama Brand:</label>
                                    @if ($errors->any())
                                    <input type="text" class="form-control {{ $errors->has('brand_name') ? 'is-invalid' : 'is-valid' }}" id="brand_name" name="brand_name" value="{{ old('brand_name') }}">
                                    @else
                                    <input type="text" class="form-control" id="brand_name" name="brand_name" >
                                    @endif
                                    @if ($errors->has('brand_name'))
                                    <div class="invalid-feedback">{{ $errors->first('brand_name') }}</div>
                                    @endif
                                </div>
                                <div class="form-group text-left">
                                    <label for="brand_foto">Brand Logo:</label>
                                    @if ($errors->any())
                                    <input type="file" class=" form-control {{ $errors->has('brand_foto') ? 'is-invalid' : 'is-valid' }}" id="brand_foto" name="brand_foto" >
                                    @else
                                    <input type="file" class="form-control" id="brand_foto" name="brand_foto">  
                                    @endif      
                                    @if ($errors->has('brand_foto'))
                                    <div class="invalid-feedback">{{ $errors->first('brand_foto') }}</div>
                                    @endif
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="reset" class="btn btn-sm btn-default" data-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-sm btn-primary">Save changes</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div> --}}
            <!-- END Regular Modal 2 -->
            <!-- Regular Modal Product -->
            {{-- <div id="modal-regular-product" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <form action="{{ route('product.store') }}" method="post" enctype="multipart/form-data">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <h3 class="modal-title">Product Form</h3>
                            </div>
                            <div class="modal-body">                                
                                {{ csrf_field() }}      
                                <div class="form-group text-left">
                                    <label for="product_name">Nama Produk:</label>
                                    @if ($errors->any())
                                    <input type="text" class="form-control {{ $errors->has('product_name') ? 'is-invalid' : 'is-valid' }}" id="product_name" name="product_name" value="{{ old('product_name') }}">
                                    @else
                                    <input type="text" class="form-control" id="name" name="name">
                                    @endif
                                    @if ($errors->has('product_name'))
                                    <div class="invalid-feedback">{{ $errors->first('product_name') }}</div>
                                    @endif
                                </div>   
                                <div class="form-group text-left">
                                    <label for="product_spec">Spesifikasi:</label>
                                    @if ($errors->any())
                                        <textarea class="form-control {{ $errors->has('product_spec') ? 'is-invalid' : 'is-valid' }}" id="product_spec" name="product_spec" value="{{ old('product_spec') }}"></textarea>
                                    @else
                                        <textarea class="form-control" id="spec" name="spec"></textarea>
                                    @endif
                                    @if ($errors->has('product_spec'))
                                        <div class="invalid-feedback">{{ $errors->first('product_spec') }}</div>
                                    @endif
                                </div>   
                                <div class="form-group text-left">
                                    <label for="id_brand">Brand:</label>
                                    @if ($errors->any())            
                                        <select name="brand" class="custom-select {{ $errors->has('id_brand') ? 'is-invalid' : 'id_brand' }}" id="id_brand" >
                                    @else
                                        <select name="brand" class="custom-select form-control"> 
                                    @endif               
                                        <option selected disabled hidden>Pilih</option>
                                        @foreach ($brand as $brans) 
                                        <option value="{{ $brans->id }}">{{ $brans->brand_name  }}</option>
                                        @endforeach
                                        </select>
                                    @if ($errors->has('id_brand'))            
                                        <div class="invalid-feedback">{{ $errors->first('id_brand') }}</div>
                                    @endif
                                </div> 
                                <div class="form-group text-left">
                                    <label for="transmision">Transmisi:</label>
                                    @if ($errors->any())
                                        <input type="text" class="form-control {{ $errors->has('transmision') ? 'is-invalid' : 'is-valid' }}" id="transmision" name="transmision" value="{{ old('transmision') }}">
                                    @else
                                        <input type="text" class="form-control" id="transmisi" name="transmisi">
                                    @endif
                                    @if ($errors->has('transmision'))
                                        <div class="invalid-feedback">{{ $errors->first('transmision') }}</div>
                                    @endif
                                </div>   
                                <div class="form-grop text-left">
                                    <label for="category_name">Category:</label>
                                    @foreach($category as $categorys)
                                    <div class="checkbox">    
                                        <input type="checkbox" name="category_name[]" value="{{$categorys->id}}"> {{$categorys->category_name}}<br>
                                    </div>
                                    @endforeach
                                </div>
                                <div class="form-group text-left">
                                    <label for="transmision">Bahan Bakar:</label>
                                    @if ($errors->any())
                                        <input type="text" class="form-control {{ $errors->has('transmision') ? 'is-invalid' : 'is-valid' }}" id="transmision" name="transmision" value="{{ old('transmision') }}">
                                    @else
                                        <input type="text" class="form-control" id="bensin" name="bensin">
                                    @endif
                                    @if ($errors->has('transmision'))
                                        <div class="invalid-feedback">{{ $errors->first('transmision') }}</div>
                                    @endif
                                </div>
                                <div class="form-group text-left">
                                    <label for="product_spec">Deskripsi:</label>
                                    @if ($errors->any())
                                        <textarea class="form-control {{ $errors->has('product_spec') ? 'is-invalid' : 'is-valid' }}" id="product_spec" name="product_spec" value="{{ old('product_spec') }}"></textarea>
                                    @else
                                        <textarea class="form-control" id="desc" name="desc"></textarea>
                                    @endif
                                    @if ($errors->has('product_spec'))
                                        <div class="invalid-feedback">{{ $errors->first('product_spec') }}</div>
                                    @endif
                                </div>
                                <div class="form-group fieldGroup text-left">
                                    <div class="input-group">
                                        <input type="text" name="color[]" class="form-control" placeholder="Warna"/>
                                        <input type="file" name="icon_color[]" class="form-control" placeholder="Icon color"/>
                                        <input type="file" name="foto_mobil[]" class="form-control" placeholder="Foto Mobil"/>
                                        <input type="text" name="harga[]" class="form-control" placeholder="Harga"/>                    
                                        <div class="input-group-addon"> 
                                            <a href="javascript:void(0)" class="btn btn-success addMore"><span class="glyphicon glyphicon glyphicon-plus" aria-hidden="true"></span> +</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="reset" class="btn btn-sm btn-default" data-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-md btn-primary submit">Submit</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div> --}}
            <!-- END Regular Modal Product -->
            <!-- Regular Modal Category -->
            {{-- <div id="modal-regular-category" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content text-left">
                        <form action="{{ route('category.store') }}" method="post" enctype="multipart/form-data">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <h3 class="modal-title">Category Form</h3>
                            </div>
                            <div class="modal-body">                                
                            {{ csrf_field() }}      
                            <div class="form-group">
                                <label for="category_name">Category:</label>
                                @if ($errors->any())
                                    <input type="text" class="form-control {{ $errors->has('category_name') ? 'is-invalid' : 'is-valid' }}" id="category_name" name="category_name" value="{{ old('category_name') }}">
                                @else
                                    <input type="text" class="form-control" id="category_name" name="category_name" >
                                @endif
                                @if ($errors->has('category_name'))
                                    <div class="invalid-feedback">{{ $errors->first('category_name') }}</div>
                                @endif
                                </div> 
                            </div>
                            <div class="modal-footer">
                                <button type="reset" class="btn btn-sm btn-default" data-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-md btn-primary submit">Submit</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div> --}}
            <!-- END Regular Modal Category -->
            {{-- <div class="col-sm-6 col-lg-3">
                <a href="/admin/product/create" data-toggle="modal" class="widget widget-hover-effect2">
                    <div class="widget-extra themed-background-success">
                        <h4 class="widget-content-light"><strong>Add New</strong> Product</h4>
                    </div>
                    <div class="widget-extra-full"><span class="h2 text-success animation-expandOpen"><i class="fa fa-plus"></i></span></div>
                </a>
            </div>
            <div class="col-sm-6 col-lg-3">
                <a href="#modal-regular-category" data-toggle="modal" class="widget widget-hover-effect2">
                    <div class="widget-extra themed-background-danger">
                        <h4 class="widget-content-light"><strong>Add New </strong> Category</h4>
                    </div>
                    <div class="widget-extra-full">
                        <span class="h2 text-danger animation-expandOpen">
                           <i class="fa fa-plus"></i>
                        </span>
                    </div>
                </a>
            </div>
            <div class="col-sm-6 col-lg-3">
                <a href="#modal-regular2" data-toggle="modal" class="widget widget-hover-effect2">
                    <div class="widget-extra themed-background-dark">
                        <h4 class="widget-content-light"><strong>Add New </strong> Brand</h4>
                    </div>
                    <div class="widget-extra-full"><span class="h2 themed-color-dark animation-expandOpen"><i class="fa fa-plus"></i></span></div>
                </a>
            </div>
            <div class="col-sm-6 col-lg-3">
                <a href="javascript:void(0)" class="widget widget-hover-effect2">
                    <div class="widget-extra themed-background-dark">
                        <h4 class="widget-content-light"><strong>All</strong> Products</h4>
                    </div>
                <div class="widget-extra-full"><span class="h2 themed-color-dark animation-expandOpen">{{$count}}</span></div>
                </a>
            </div>
        </div> --}}
        <!-- END Quick Stats -->
        <!-- Datatables Content -->
        <div class="block full">
            <div class="block-title">
                <h2><strong>Daftar</strong> Produk</h2>
            </div>
            <div class="table-responsive">
                <table id="ecom-products" class="table table-bordered table-striped table-vcenter">
                    <thead>
                        <tr>
                            <th class="text-center" style="width: 70px;">ID</th>
                            <th class="hidden-xs text-center">Produk</th>
                            <th class="hidden-xs text-center">Kategori</th>
                            <th class="hidden-xs text-center">Brand</th>
                            <th class="hidden-xs text-center">Harga</th>
                            <th class="hidden-xs text-center">Gambar</th>
                            <th class="text-center">Action</th>
                        </tr>
                    </thead>
                    {{-- <tbody>
                        @foreach($produk as $dataproduk)
                            <tr>
                                <td class="text-center">{{$dataproduk->id}}</td>
                                <td class="text-center">{{$dataproduk->name}}</td>
                                <td class="text-center">{{$dataproduk->category}}</td>
                                <td class="text-center">{{$dataproduk->brand}}</td>
                                <td class="text-center"><img src="{{asset('gambar/'.$dataproduk->image)}}" width="80px" height="60px"></td>
                                <td>
                                    <div class="btn-group btn-group-xs form-inline">
                                        <form action="{{ route('product.destroy', $dataproduk->id) }}" method="post">
                                            {{ csrf_field() }}                                                                    
                                            {{ method_field('DELETE') }}
                                            <a href="{{ url('admin/product-edit?id='.$dataproduk->id) }}" data-toggle="tooltip" title="Edit" class="btn btn-xs btn-primary"><i class="fa fa-pencil"></i></a>
                                            <a href="{{url('admin/edit-image-product?id='.$dataproduk->id)}}" data-toggle="tooltip" title="Edit Image" class="btn btn-xs btn-default"><i class="fa fa-image"></i></a>
                                            <button data-toggle="tooltip" title="Delete" class="btn btn-xs btn-danger" type="submit" onclick="return confirm('Yakin ingin menghapus data?')"><i class="fa fa-times"></i></button>
                                        </form>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    </tbody> --}}
                </table>
            </div>
        </div>
<script src="{{asset('proui/backend/js/vendor/jquery.min.js') }}"></script>
<script src="{{asset('proui/backend/js/vendor/bootstrap.min.js') }}"></script>
<script src="{{asset('proui/backend/js/pages/ecomProducts.js') }}"></script>
<script>$(function(){ EcomProducts.init(); });</script>
<script src="{{asset('proui/backend/js/pages/tablesDatatables.js') }}"></script>
<script>$(function(){ TablesDatatables.init(); });</script>               
@endsection